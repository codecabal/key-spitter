#!/usr/bin/python3.2

from sys import stderr
from sys import exit
import argparse
from datetime import datetime


charset = []
lowAlpha  = [ "e", "t", "a", "o", "i", "n", "s", "h", "r", "d", "l", "u", "c", "m", "f", "w", "y", "p", "v", "b", "g", "k", "j", "q", "x", "z"]
highAlpha = [ "E", "T", "A", "O", "I", "N", "S", "H", "R", "D", "L", "U", "C", "M", "F", "W", "Y", "P", "V", "B", "G", "K", "J", "Q", "X", "Z"]
numeric = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
special = [" ", "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=", "<", ">", ",", ".", "/", "?", ";", ":", "'", "\"", "\\", "|", "[", "]", "{", "}"]
wrout = print
wrst  = stderr.write

def add_characters(list, chars):
    """Adds characters to the charset.

    Appends each element of chars to list.
    Parameters:
    list  - a list of characters
    chars - a list of characters

    """
    for c in chars[:]:
        list.append(c)

def generate_one_word(cw):
    """Generates the next permutation after the given word.

    Parameter:
    cw - string, contains the previous permutation.
    return - string, contains the next permutation.

    """
    nval = ""
    if cw[0] == charset[len(charset) - 1]:
        carry = True
        nval = charset[len(charset) - 1] + cw[1:]
    else:
        carry = False
        nval = charset[charset.index(cw[0]) + 1] + cw[1:]
    cw = nval
    j = 0
    while carry:
        carry = False
        #if current char not at the end of the word
        if j < len(cw) - 1:
            nval = ""
            #current char index +1 is over the charset size
            if charset.index(cw[j]) + 1 > len(charset) - 1:
                nval = charset[0]
                carry = True
            else:
                #current char index + 1 is still in charset
                nval = charset[charset.index(cw[j]) + 1]
            cw = cw[:j] + nval + cw[j + 1:]
        else:
        #if current char at the end of the word
            nval = ""
            #if current char is over last in the charset
            if charset.index(cw[j]) + 1 > len(charset) - 1:
                nval = charset[0] + charset[0]
            else:
                #if current char not last in the charset
                nval = charset[charset.index(cw[j]) + 1]
            cw = cw[:j] + nval
        j = j + 1
    return cw

def print_status(i):
    """Prints progress message.

    Prints the progress message of the permutation generation.
    Parameters:
    i - int, the number of permutations generated.

    """
    wrst(str(datetime.time(datetime.now())) + ":> " + str(i) + " words were generated.\n")

def print_session_statistics(i, std, cw):
    """Prints information about the current session.

    Prints last word that was generated the time it took to generate
    the words and the number of words generated.
    The function is usually called at termination to specify what word was last
    generated, to be able to continue from there.
    Parameters:
    i - int, the number of words that were generated.
    std - datetime, the date and time when the generation started.
    cw - string, last word generated.

    """
    total = datetime.now() - std
    wrst("\n------------------------------------------\n")
    wrst("Last word generated: " + cw + "\n")
    wrst("Generated " + str(i) + " words, in: " + str(total.days) + " days, " + str((total.seconds // 3600) % 24) + " hours, " + str((total.seconds // 60) % 60)  + " minutes, " + str(total.seconds % 60) + " seconds, " + str(total.microseconds % 1000) + " milliseconds\n")
    wrst("------------------------------------------\n")


def generate(args, charset):
    """Generates permutation sequences from the characters in charset.

    Parameters:
    args - argparse.ArgumentParser, command line arguments.
    charset - list, contains all the characters that can be used to generate the permutations.

    """
    try:
        status_update_interval = 10000000
        startdate = datetime.now()
        if args.start is None:
            currentWord = charset[0]
        else:
            currentWord = args.start
        if args.length > 0:
            i = 1
            wrout(currentWord)
            while len(currentWord) <= args.length:
                currentWord = generate_one_word(currentWord)
                wrout(currentWord)
                if i % status_update_interval == 0 and args.verbose:
                    print_status(i)
                i = i + 1
            print_session_statistics(i, startdate, currentWord)
        elif args.end != None:
            i = 1
            wrout(currentWord)
            while currentWord != args.end:
                currentWord = generate_one_word(currentWord)
                wrout(currentWord)
                if i % status_update_interval == 0 and args.verbose:
                    print_status(i)
                i = i + 1
            print_session_statistics(i, startdate, currentWord)
        elif args.count <= 0:
            i = 1
            wrout(currentWord)
            while True:
                currentWord = generate_one_word(currentWord)
                wrout(currentWord)
                if i % status_update_interval == 0 and args.verbose:
                    print_status(i)
                i = i + 1
            print_session_statistics(i, startdate, currentWord)
        elif args.count > 0:
            i = 1
            wrout(currentWord)
            while i < args.count:
                currentWord = generate_one_word(currentWord)
                wrout(currentWord)
                if i % status_update_interval == 0 and args.verbose:
                    print_status(i)
                i = i + 1
            print_session_statistics(i, startdate, currentWord)
    except(KeyboardInterrupt, SystemExit):
        print_session_statistics(i, startdate, currentWord)
        wrst("Caught keyboard interrupt. Exiting..Bye!\n")
        exit(0)

def add_charsets(args, charset, lowAlpha, highAlpha, numeric, special):
    """Specifies the charasets to be used for permutation generation.

    Adds all charsets specified on the command line to the set that will be
    used to generate the permutations.
    Parameters:
    args - argparse.ArgumentParser, contains the command line arguments.
    charset - list, after function finishes will contain the characters used for permutation generation.
    lowAlpha - list, contains small letters.
    highAlpha - list, contains all capital letters.
    numeric - list, contains digits from 0 to 9.
    special - list, contains all special characters.

    """
    if "all" in args.charsets:
        add_characters(charset, lowAlpha)
        add_characters(charset, highAlpha)
        add_characters(charset, numeric)
        add_characters(charset, special)
    else:
        if("la" in args.charsets):
            add_characters(charset, lowAlpha)
        if("ha" in args.charsets):
            add_characters(charset, highAlpha)
        if("n" in args.charsets):
            add_characters(charset, numeric)
        if("s" in args.charsets):
            add_characters(charset, special)

def end_greater_than_start(chars, st, en):
    """Check if the starting word is greater than the ending word.

    Arguments:
    chars - list, contains the characters used to gernerate the permutations.
    st - string, starting word.
    en - string, last word.

    """
    if(len(st) > len(en)):
        return False
    if(len(en) > len(st)):
        return True
    for i in range(len(st)):
        if chars.index(st[i]) > chars.index(en[i]):
            return False
        elif chars.index(en[i]) > chars.index(st[i]):
            return True

def main(args):
    if args.printcharsets:
        wrst("""If if more than one are selected, the character sets will be concatenated in the order presented below.""" + "\n\n")
        wrst("Low alpha: " + str(lowAlpha) + "\n")
        wrst("High alpha: " + str(highAlpha) + "\n")
        wrst("Numeric: " + str(numeric) + "\n")
        wrst("Special: " + str(special) + "\n")
        exit(0)
    add_charsets(args, charset, lowAlpha, highAlpha, numeric, special)
    if args.start is not None:
        for i in range(len(args.start)):
            if args.start[i] not in charset:
                wrst("Error: Not all characters in the start word are in the charset! Exiting!")
                exit(0)
    if args.end is not None:
        for i in range(len(args.end)):
            if args.end[i] not in charset:
                wrst("Error: Not all characters in the start end are in the charset! Exiting!")
                exit(0)
    if (args.start is not None) and (args.end is not None):
        if end_greater_than_start(charset, args.start, args.end):
            generate(args, charset)
        else:
            wrst("error: starting word must precede end word in the generation order.\n")
            exit(0)
    else:
        generate(args, charset)

def process_command_line_options():
    """Gathers command line options.

    Returns - argparse.ArgumentParser, contains valid command line options.

    """
    parser = argparse.ArgumentParser(description="""
            Generates permutations of characters from the specified character lists.
            By default the application prints the words to standard out and prints
            status updates to standard error.
            If none of the limiting options(length, count, stop) are specified the
            application generates words until it is closed.
        """)
    parser.add_argument("-s", "--start", type=str, help="""
            Keyword from where to start the word generation.
        """)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-e", "--end", type=str, help="""
            Stop generating after specified word.
        """)
    group.add_argument("-l", "--length", type=int, help="""
            Generate all words up until the specified length. Default is %(default)s disabling max length restriction.
         """, default="-1")
    group.add_argument("-c", "--count", type=int, help="""
            Stops generating new words after the specified count were generated.
            Default is %(default)s, word generation stopping only if the application is closed.
        """, default="-1")
    parser.add_argument("-ch", "--charsets", nargs="+", choices=["all" ,"la", "ha", "n", "s"], type=str, help="""
            Use specified charsets to generate words. The options are: low alpha,
            high alpha, numeric, specia, all. Defaults to %(default)s.
        """, default="all")
    parser.add_argument("-v", "--verbose", action="store_true", help="""
            Print progress updates from time to time. Default is %(default)s.
        """)
    parser.add_argument("--version", action="version", version="%(prog)s 0.1")
    parser.add_argument("-pc", "--printcharsets", action="store_true", help=
        """Prints all built-in character sets and exits.""")
    args = parser.parse_args()
    return args

def set_output_function(wro, wrs):
    wro = print
    wrs = stderr.write

if __name__ == "__main__":
    set_output_function(wrout, wrst)
    main(process_command_line_options())